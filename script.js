let billAmount = 0;
let numPeople = 0;

$(".add-people").click(function(){
    $(".people").text(++numPeople);
});

$(".remove-people").click(function(){
    --numPeople;
    numPeople < 0 ? (numPeople = 0) : numPeople;

    $(".people").text(numPeople);
});

let tipPercentage = 0;

let tips = document.querySelectorAll(".tip-button");

tips.forEach(element => {
    element.addEventListener("click", () =>{
        tipPercentage = element.value;
        tips.forEach(btn => {
            btn.classList.remove("tip-button-active");
        });
        element.classList.add("tip-button-active");
    })
});


let generateBtn = $(".generate");

generateBtn.click(function(){

    billAmount = $("input:text").val() ;

    if (generateBtn.text() == "GENERATE BILL" && billAmount>0) {
        //Tip Amount Per Person
        let tip = Math.floor(((tipPercentage / 100) * billAmount) / numPeople)
        
        $(".tip-amount").text("₹ " + tip + ".00");

        //Total Amount Per Person
        let totalAmount = Math.floor(billAmount / numPeople) + tip;

        $(".total").text("₹ " + totalAmount + ".00");

        generateBtn.text("RESET");

    } else if (generateBtn.text() == "RESET") {
        tipPercentage = 0;
        numPeople = 0;
        billAmount = 0;

        $(".tip-amount").text ("₹ " + "0.00");
        $(".total").text("₹ " + "0.00");

        generateBtn.text("GENERATE BILL");

        document.querySelector("input").value = ""
        tips.forEach(nav => {
            nav.classList.remove("tip-button-active");
        })

        $(".people").text(numPeople);
    }
})